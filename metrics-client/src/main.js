import { createApp } from 'vue'
import App from './App.vue'
import { library } from '@fortawesome/fontawesome-svg-core'
import {
    faTrashAlt,
    faEdit,
    faUserPlus,
    faPlus,
    faUser,
    faSignOutAlt,
    faTimes,
    faChartBar,
    faUsers,
    faList,
    faColumns
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import router from './router'
import store from './store'
import './assets/style/style.scss'

library.add(faTrashAlt, faEdit, faUserPlus, faPlus, faUser, faSignOutAlt, faTimes, faChartBar, faUsers, faList, faColumns)

store.dispatch('users/loginAfterRefresh')

createApp(App)
    .use(store)
    .use(router)
    .component('font-awesome-icon', FontAwesomeIcon)
    .mount('#app')
