import { createRouter, createWebHistory } from 'vue-router'
import Login from "@/views/Login";
import DashboardPage from "@/views/DashboardPage";
import ChartsPage from "@/views/ChartsPage";
import UsersPage from "@/views/UsersPage";
import InstancesPage from "@/views/InstancesPage";
import NotFoundPage from "@/views/NotFoundPage";

const router = createRouter({
  history: createWebHistory(),
  routes: [
    {path: '/login', name: 'Login', component: Login, alias: '/'},
    {path: '/dashboard', name: 'Dashboard', component: DashboardPage, meta: {requiresAuth: true}},
    {path: '/charts', name: 'Charts', component: ChartsPage, meta: {requiresAuth: true}},
    {path: '/users', name: 'Users', component: UsersPage, meta: {requiresAuth: true}},
    {path: '/instances', name: 'Instances', component: InstancesPage, meta: {requiresAuth: true}},
    {path: '/:notFound(.*)*', name: 'NotFound', component: NotFoundPage},
  ],
  linkActiveClass: 'active',
  linkExactActiveClass: 'active'
})

router.beforeEach((to, from, next) => {
  const currentUser = localStorage.getItem('token')
  const checkAuth = to.matched.some(record => record.meta.requiresAuth)

  if (checkAuth && !currentUser) {
    next('/login')
  } else {
    next()
  }
})

export default router
