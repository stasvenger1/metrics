import { createStore } from 'vuex'
import users from "./modules/users"
import instances from "./modules/instances"

export default createStore({
  modules: {
    users,
    instances
  }
})
