import axios from "@/services/axiosBase"

export default ({
    namespaced: true,
    state: {
        instances: []
    },
    mutations: {
        SET_INSTANCES: (state, instances) => {
            state.instances = instances
        },
        ADD_INSTANCE: (state, instance) => {
            state.instances.push(instance)
        },
        DELETE_INSTANCE: (state, instanceId) => {
            state.instances = state.instances.filter(instance => instance._id !== instanceId)
        }
    },
    actions: {
        getInstances({commit}) {
            return axios.get('instances')
                .then(response => {
                    commit('SET_INSTANCES', response.data)
                    return response
                })
                .catch(error => {
                    console.log(error)
                    return error
                })
        },
        addInstance({commit}, instanceData) {
            return axios.post('instances', instanceData)
                .then(response => {
                    commit('ADD_INSTANCE', response.data)
                    return response
                })
                .catch(error => {
                    console.log(error)
                    return error
                })
        },
        deleteInstance({commit}, instanceId) {
            return axios.delete('instances/' + instanceId)
                .then(() => {
                    commit('DELETE_INSTANCE', instanceId)
                    return instanceId
                })
                .catch(error => {
                    console.log(error)
                    return error
                })
        }
    },
    getters: {
        getInstancesList(state) {
            return state.instances
        }
    }
})