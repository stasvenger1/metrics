import axios from "@/services/axiosBase"
import router from '@/router/index.js'

export default ({
    namespaced: true,
    state: {
        user: null,
        token: null,
        users: []
    },
    mutations: {
        SET_USER: (state, data) => {
            state.user = data
        },
        SET_TOKEN: (state, token) => {
            state.token = token
        },
        SIGN_OUT: (state) => {
            state.user = null
            state.token = null
        },
        ADD_USER: (state, userInfo) => {
            state.users.push(userInfo)
        },
        SET_USERS: (state, users) => {
            state.users = users
        },
        DELETE_USER: (state, userId) => {
            state.users = state.users.filter(user => user._id !== userId)
        }
    },
    actions: {
        signIn ({commit}, credentials) {
            return axios.post('users/sessions', credentials)
                .then(response => {
                    localStorage.setItem('token', JSON.stringify(response.data.token))
                    localStorage.setItem('user', JSON.stringify(response.data.user))
                    commit('SET_TOKEN', response.data.token)
                    commit('SET_USER', response.data.user)
                    router.push({name: 'Dashboard'})
                })
                .catch(error => {
                    console.log(error)
                    return error
                })
        },
        loginAfterRefresh(context) {
            let token = localStorage.getItem('token')
            let user = localStorage.getItem('user')
            if (token) {
                context.commit('SET_TOKEN', JSON.parse(token))
                context.commit('SET_USER', JSON.parse(user))
            }
        },
        getUsersList ({commit}) {
            return axios.get('users')
                .then(response => {
                    commit('SET_USERS', response.data)
                    return response
                })
                .catch(error => {
                    console.log(error)
                    return error
                })
        },
        addUser ({commit}, userInfo) {
            return axios.post('users', userInfo)
                .then(response => {
                    commit('ADD_USER', response.data)
                    return response
                })
                .catch(error => {
                    console.log(error)
                    return error
                })
        },
        signOut ({commit}, token) {
            return axios.delete('users/sessions', token)
                .then(() => {
                    localStorage.removeItem('user')
                    localStorage.removeItem('token')
                    commit('SIGN_OUT', token)
                    router.push({name: 'Login'})
                })
                .catch(error => {
                    console.log(error)
                    return error
                })
        },
        deleteUser ({commit}, userId) {
            return axios.delete('users/' + userId)
                .then(() => {
                    commit('DELETE_USER', userId)
                })
                .catch(error => {
                    console.log(error)
                    return error
                })
        }
    },
    getters: {
        user (state) {
            return state.user
        },
        token (state) {
            return state.token
        },
        usersList (state) {
            return state.users
        }
    }
})
