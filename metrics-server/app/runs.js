const express = require('express');
const Runs = require('../models/Runs');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const router = express.Router();

router.get('/', auth, async (req, res) => {
   try {
       const runs = await Runs.find()
       return res.send(runs);
   } catch (e) {
       return res.status(500).send(e)
   }
});

router.post('/', [auth, permit('admin')], (req, res) => {
   const run = new Runs(req.body);
   run.save()
       .then(result => res.send(result))
       .catch(e => res.status(400).send(e))
});

router.delete('/:id', [auth, permit('admin')], (req, res) => {
    Runs.deleteOne({_id: req.params.id})
        .then(result => res.send(result))
        .catch(e => res.status(403).send(e))
});

module.exports = router;