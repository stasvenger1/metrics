const express = require('express');
const jwt = require('jsonwebtoken');
const {secret} = require('../config');
const User = require('../models/User');
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");

const router = express.Router();

router.get('/', auth, async (req, res) => {
    try {
        const users = await User.find({}, {
            username: true,
            role: true});

        return res.send(users)
    } catch (e) {
        return res.status(500).send(e)
    }
});

router.post('/', [auth, permit('admin')], (req, res) => {
    const userData = {
        username: req.body.username,
        role: req.body.role,
        password: req.body.password,
    };

    const user = new User (userData);

    user.save()
        .then(results => res.send(results))
        .catch(error => {
            res.status(400).send(error)
        } );
})

router.post('/sessions', async (req, res) => {
   try {
        const { username, password } = req.body;
        const user = await User.findOne({username});
        if(!user) {
            return res.status(401).json({message: 'User not found'})
        }
        const isPasswordValid = user.checkPassword(password);
        if(!isPasswordValid) {
            return res.status(401).json({message: 'Username or password is not correct'})
        }
        const token = jwt.sign({_id: user._id, role: user.role}, secret, {expiresIn: "6h"});

        return res.json({
            token,
            user: {
                _id: user._id,
                username: user.username,
                role: user.role
            }
        })

   } catch (error) {
       res.status(401).send(error)
   }
});

router.delete('/sessions', auth, async (req, res) => {
    const token = req.get('Authorization');
    const success = {message: 'Logged out'};
    if(!token) {
        return res.send(success);
    }
    const user = await User.findOne({token});
    if(!user) {
        return res.send(success);
    }
    await user.save();
    return res.send(success);
});

router.delete('/:id', [auth, permit('admin')], (req, res) => {
    User.deleteOne({_id: req.params.id})
        .then(result => res.send(result))
        .catch(error => res.status(403).send(error))
})

module.exports = router;