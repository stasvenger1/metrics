const express = require('express');
const TestCases = require('../models/TestCases');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const router = express.Router();

router.get('/', auth, async (req, res) => {
   try {
       const testCases = await TestCases.find().populate('testArea', '_id name');
       return res.send(testCases);
   } catch (e) {
       return res.status(500).send(e)
   }
});

router.post('/', [auth, permit('admin')], (req, res) => {
    const testCase = new TestCases(req.body);
    testCase.save()
        .then(result => res.send(result))
        .catch(e => res.status(400).send(e))
});

router.delete('/:id', [auth, permit('admin')], (req, res) => {
    TestCases.deleteOne({_id: req.params.id})
        .then(result => res.send(result))
        .catch(e => res.status(403).send(e))
});

module.exports = router;