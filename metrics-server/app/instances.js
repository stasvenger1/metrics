const express = require('express');
const Instance = require('../models/Instance');
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");

const router = express.Router();

router.get('/', auth, async (req, res) => {
    try {
        const instances = await Instance.find()
        return res.send(instances)
    } catch (e) {
        return res.status(500).send(e)
    }
});

router.post('/', [auth, permit('admin')], (req, res) => {
    const instanceData = {
        instance: req.body.instance
    };

    const instance = new Instance(instanceData);

    instance.save()
        .then(results => res.send(results))
        .catch(error => {
            res.status(400).send(error)
        });
});

router.delete('/:id', [auth, permit('admin')], (req, res) => {
    Instance.deleteOne({_id: req.params.id})
        .then(result => res.send(result))
        .catch(error => res.status(403).send(error))
});


module.exports = router;