const express = require('express');
const TestRun = require('../models/TestRun');
const auth = require('../middleware/auth');

const router = express.Router();

router.get('/', auth, async (req, res) => {
   try {
       const testRuns = await TestRun.find()
           .populate('user', '_id username')
           .populate('runs', '_id')
           .populate('browser', '_id name')
           .populate('instance', '_id instance')
           .populate('testArea', '_id name')
           .populate('testCase', '_id name')
       return res.send(testRuns);
   } catch (e) {
       return res.status(500).send(e);
   }
});

router.post('/', auth, (req, res) => {
    const testRunData = {
        ...req.body,
        dateTime: new Date().toISOString()
    };
    const testRun = new TestRun(testRunData);
    testRun.save()
       .then(result => res.send(result))
       .catch(e => res.status(400).send(e))
});

module.exports = router;