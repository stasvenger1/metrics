const express = require('express');
const TestArea = require('../models/TestAreas');
const auth = require('../middleware/auth');
const permit = require('../middleware/permit');

const router = express.Router();

router.get('/', auth, async (req, res) => {
   try {
       const testAreas = await TestArea.find();
       return res.send(testAreas);
   } catch (e) {
       return res.status(500).send(e);
   }
});

router.post('/', [auth, permit('admin')], (req, res) => {
    const testAreaData = {
        name: req.body.name
    };
    const testArea = new TestArea(testAreaData);
    testArea.save()
        .then(result => res.send(result))
        .catch(e => res.status(400).send(e))
});

router.delete('/:id', [auth, permit('admin')], (req, res) => {
    TestArea.deleteOne({_id: req.params.id})
        .then(result => res.send(result))
        .catch(e => res.status(403).send(e))
});

module.exports = router;