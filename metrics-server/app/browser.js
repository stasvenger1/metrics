const express = require('express');
const Browser = require('../models/Browsers');
const auth = require("../middleware/auth");
const permit = require("../middleware/permit");

const router = express.Router();

router.get('/', auth, async (req, res) => {
    try {
        const browsers = await Browser.find()
        return res.send(browsers)
    } catch (e) {
        return res.status(500).send(e)
    }
});

router.post('/', [auth, permit('admin')], (req, res) => {
    const browserData = {
        name: req.body.name
    };
    const browser = new Browser(browserData);
    browser.save()
        .then(result => res.send(result))
        .catch(e => res.status(400).send(e));
});

router.delete('/:id', [auth, permit('admin')], (req, res) => {
    Browser.deleteOne({_id: req.params.id})
        .then(result => res.send(result))
        .catch(e => res.status(403).send(e))
});


module.exports = router;