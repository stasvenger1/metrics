const mongoose = require('mongoose');
const config = require('./config');
const User = require('./models/User');

const run = async () => {
    await mongoose.connect(config.dbUrl, config.mongoOptions);

    const connection = mongoose.connection;

    const collections = await connection.db.collections();

    for (let collection of collections) {
        await collection.drop();
    }

    await User.create(
        {
            username: 'metricsAdmin',
            password: '123456789',
            role: 'admin'
        },
        {
            username: 'metricsUser',
            password: '123456789',
        }
    );

    return connection.close();
};

run().catch(error => {
    console.error('Something wrong happened...', error);
});
