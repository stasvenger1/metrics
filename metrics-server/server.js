const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');
const config = require('./config');

const users = require('./app/users');
const instances = require('./app/instances');
const browser = require('./app/browser');
const testArea = require('./app/testArea');
const testCase = require('./app/testCase');
const runs = require('./app/runs');
const testRun = require('./app/testRun');

const app = express();

app.use(cors());
app.use(express.json());
app.use(express.static('public'));

let port = 8000;

if (process.env.NODE_ENV === 'production') {
    port = 8008;
}

mongoose.connect(config.dbUrl, config.mongoOptions).then(() => {
    app.use('/users', users);
    app.use('/instances', instances);
    app.use('/browser', browser);
    app.use('/test-area', testArea);
    app.use('/test-case', testCase);
    app.use('/runs', runs);
    app.use('/test-run', testRun);

    app.listen(port, () => {
        console.log(`Server started on ${port} port`);
    });
});

