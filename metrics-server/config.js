const rootPath = __dirname;

const dbUrl = 'mongodb://localhost/metrics';
const secret = 'AUTOMATION_METRICS_SECRET_KEY';

module.exports = {
    rootPath,
    dbUrl,
    secret,
    mongoOptions: {
        useNewUrlParser: true,
    }
};