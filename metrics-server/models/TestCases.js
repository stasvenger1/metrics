const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const TestCaseSchema = new Schema ({
    testArea: {
        type: Schema.Types.ObjectId,
        ref: 'TestArea',
        required: [true, 'Required field'],
    },
    name: {
        type: String,
        unique: true,
        required: [true, 'Required field'],
        validate: {
            validator: async  function (value) {
                if (!this.isModified('browser')) return;
                const testCase = await TestCase.findOne({name: value});
                if (testCase) throw new Error(`Browser ${value} already in use!`);
            }
        }
    }
});

const TestCase = mongoose.model('TestCase', TestCaseSchema);

module.exports = TestCase;