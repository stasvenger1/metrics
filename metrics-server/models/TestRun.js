const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const TestRunSchema = new Schema ({
    runs: {
        type: Schema.Types.ObjectId,
        ref: 'Runs',
        required: [true, 'Required field'],
    },
    user: {
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: [true, 'Required field'],
    },
    browser: {
        type: Schema.Types.ObjectId,
        ref: 'Browser',
        required: [true, 'Required field'],
    },
    instance: {
        type: Schema.Types.ObjectId,
        ref: 'Instance',
        required: [true, 'Required field'],
    },
    testArea: {
        type: Schema.Types.ObjectId,
        ref: 'TestArea',
        required: [true, 'Required field'],
    },
    testCase: {
        type: Schema.Types.ObjectId,
        ref: 'TestCase',
        required: [true, 'Required field'],
    },
    timing: {
        type: Number,
        required: true
    },
    dateTime: {
        type: Date,
    }
});

const TestRun = mongoose.model('TestRun', TestRunSchema);

module.exports = TestRun;