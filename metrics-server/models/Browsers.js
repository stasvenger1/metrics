const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const BrowserSchema = new Schema ({
    name: {
        type: String,
        unique: true,
        required: [true, 'Required field'],
        validate: {
            validator: async  function (value) {
                if (!this.isModified('browser')) return;
                const browser = await Browser.findOne({name: value});
                if (browser) throw new Error(`Browser ${value} already in use!`);
            }
        }
    }
});

const Browser = mongoose.model('Browser', BrowserSchema);

module.exports = Browser;