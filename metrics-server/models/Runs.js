const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const RunsSchema = new Schema ({
    state: {
        type: String,
        default: 'New',
        enum: ['New', 'In Progress', 'Complete']
    },
    baseline: {
        type: Boolean,
        default: false
    }
});

const Runs = mongoose.model('Runs', RunsSchema);

module.exports = Runs;