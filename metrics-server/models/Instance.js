const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const InstanceSchema = new Schema ({
    instance: {
        type: String,
        unique: true,
        required: [true, 'Required field'],
        validate: {
            validator: async  function (value) {
                if (!this.isModified('instance')) return;
                const instance = await Instance.findOne({instance: value});
                if (instance) throw new Error(`Instance ${value} already in use!`);
            }
        }
    }
});

const Instance = mongoose.model('Instance', InstanceSchema);

module.exports = Instance;