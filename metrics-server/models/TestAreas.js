const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const TestAreaSchema = new Schema ({
    name: {
        type: String,
        unique: true,
        required: [true, 'Required field'],
        validate: {
            validator: async  function (value) {
                if (!this.isModified('name')) return;
                const testArea = await TestArea.findOne({name: value});
                if (testArea) throw new Error(`Test area ${value} already in use!`);
            }
        }
    }
});

const TestArea = mongoose.model('TestArea', TestAreaSchema);

module.exports = TestArea;